# ImageMagick

An Ansible role to install ImageMagick and configure the [security policy](https://www.imagemagick.org/script/resources.php) on Debian.

By default this role will enable the reading and writing of PDF files, when `imagemagick` is `True`, it defaults to `False`.

The `imagemagick_policymap.policy` list is in Ansible [from_xml](https://docs.ansible.com/ansible/latest/collections/ansible/utils/from_xml_filter.html) format as the [to_xml](https://docs.ansible.com/ansible/latest/collections/ansible/utils/to_xml_filter.html) filter is used to template `/etc/ImageMagick-6/policy.xml`.

## Notes

Note that the `from_xml` can't be used to read the default `/etc/ImageMagick-6/policy.xml` file as it is invalid XML, a closing comment `--->` needs appending at line 76.

## Copyright

Copyright 2024 Chris Croome, &lt;[chris@webarchitects.co.uk](mailto:chris@webarchitects.co.uk)&gt;.

This role is released under [the same terms as Ansible itself](https://github.com/ansible/ansible/blob/devel/COPYING), the [GNU GPLv3](LICENSE).
